<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Http\Requests\PostRequest;
use Illuminate\Support\Facades\Storage;
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //hola
        $posts = Post::all();
        return view ('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
       // salvar 
        $post = Post::create([
            'user_id' => auth()->user()->id] + $request->all());
        $request->validate([

        'title'=>'required|min:3|max:20',
        'file' =>'image|mimes:jpg,jpeg,gif,png,svg|max:2048',
        'body' =>'required'
        //'iframe'=>'required' 

        ]);
        
       //imagen
        if ($request->file('file')){
            $imagenes= $request->file('file')->store('posts', 'public');
            $url=storage::url($imagenes);
            $post->image = $imagenes;
            $post->save();  
        }


       //retornar  
        return back()->with('status', 'Creado con éxito');
        // status es la variable de configuración que tengo en la vista blade si status tiene valor entonces imprime
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
  

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($post)
    {
        $posts = Post::find($post);
        return view('posts.edit',  compact('posts'));
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */

        public function update(PostRequest $request, $post)
    {
        $post = Post::find($post);
        $post->title= $request->title;
        $post->iframe= $request->iframe;
        $post->body= $request->body;
        $post->user_id=auth()->user()->id;;
        $post->image = $request->file('file');
        if ($request->file('file')) {
            Storage::disk("public")->delete((string)$post->image);
            $post->image = $request->file('file')->store('posts', 'public'); 
         }
         
        $post->update();
        return back()->with('status', 'Actualizado con éxito');
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($post)
    {
        $post = Post::find($post);
        Storage::disk("public")->delete((string) $post->image);
        $post->delete();
        return back()->with('status', 'Eliminado con exito');
    }
}
