@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Articulos 2
                    <a href="{{ route('postsc.create') }}" class="btn btn-sm btn-primary float-right">Crear</a>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table class="table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Título</th>
                                <th colspan="2">&nbsp;</th>
                            </tr>        
                        </thead>
                        <tbody>
                            @foreach($posts as $post)
                            <tr>
                                <td>{{ $post->id }}</td>
                                <td>{{ $post->title }}</td>
                                <td>
                                      
                                    <a href="{{route('postsc.edit',$post)}}" class="btn btn-primary btn-sm">
                                        Editar
                                    </a>
                                </td>
                                <td>
                                    <form action="{{ route('postsc.destroy', $post) }}"  method="POST">
                                        @csrf
                                        @method ('DELETE')
                                        <input 
                                            type="submit" 
                                            value="Eliminar" 
                                            class="btn btn-sm btn-danger"
                                            onclick="return confirm('¿Desea Eliminar...?')" 
                                        >
                                    </form>
                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                        
                    </table>

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection